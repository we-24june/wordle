import requests as rq
import random

DEBUG = False

class WBot:
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        with open("words.txt") as f:
            self.words = [word.strip() for word in f]

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(WBot.creat_url, json=creat_dict)

        self.choices = [w for w in self.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = feedback == "GGGGG"
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']
        attempt = 1

        while not won and attempt < 6:
            if DEBUG:
                print(choice, feedback, self.choices[:10])

            self.update(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
            attempt += 1

        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, feedback: str):
        for i, t in enumerate(zip(choice, feedback)):
            if t[-1] == "Y":
                self.choices = [w for w in self.choices if t[0] in w]
            elif t[-1] == "G":
                self.choices = [w for w in self.choices if w[i] == t[0]]
            elif t[-1] == "R":
                self.choices = [w for w in self.choices if t[0] not in w]

game = WBot("Winners")
game.play()

